import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeptunLoginComponent } from './neptun-login.component';

describe('NeptunLoginComponent', () => {
  let component: NeptunLoginComponent;
  let fixture: ComponentFixture<NeptunLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NeptunLoginComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NeptunLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});