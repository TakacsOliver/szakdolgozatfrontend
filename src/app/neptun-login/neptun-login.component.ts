import { Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Institution } from '../models/InstitutionModel';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EMPTY, catchError, first, of, firstValueFrom, tap, finalize, from, map } from 'rxjs';
import { AuthenticationResponse } from '../models/AuthenticationResponseModel';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-neptun-login',
  templateUrl: './neptun-login.component.html',
  styleUrls: ['./neptun-login.component.css']
})
export class NeptunLoginComponent implements OnInit {
  institutions: Institution[] = [];
  loginForm: FormGroup = new FormGroup({});
  loginFailed: boolean = false;
  errorMessage: string = '';
  timerId: any;


  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private dataService: DataService,
    private httpClient: HttpClient,
    private _ngZone: NgZone,
  ) {}

  ngOnInit(): void {
    this.createLoginForm();
    this.loadInstitutions();
  }

  createLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      neptun: ['', Validators.required],
      password: ['', Validators.required],
      institutionSelect: ['', Validators.required],
      role: ['', Validators.required]
    });
  }

  redirectToPage(): void {
    this.router.navigate(['/login']);
  }

  closeNotificationAfterTimeout() {
    this.timerId = setTimeout(() => {
      this.closeNotification();
    }, 15000); // 15 másodperc után hívja meg a closeNotification függvényt
  }

  closeNotification() {
    this.loginFailed = false;
    clearTimeout(this.timerId);
  }

  onSubmit(): void {
    if (this.loginForm.invalid) {
      // Ellenőrizze a mezőket, amelyek hibásak vagy üresek
      const controls = this.loginForm.controls;
      for (const field in controls) {
        if (controls[field].invalid) {
          // Ellenőrizze, hogy a mező hibás vagy üres-e, és jelenítse meg az üzenetet
          controls[field].markAsTouched();
          controls[field].markAsDirty();
        }
      }
      return;
    }

    const jsonData = {
      "neptun": this.loginForm.value.neptun,
      "password": this.loginForm.value.password,
      "Url": this.loginForm.value.institutionSelect,
      // Role: this.loginForm.value.role
    };



    const url = environment.apiUrl + "LoginWithNeptun";

    from(this.httpClient.post<AuthenticationResponse>(url, jsonData)).pipe(
      first(),
      catchError((error: HttpErrorResponse) => {
        console.log(error);
        this.loginFailed = true;
        if (error.status === 0) {
          this.errorMessage = "Website not available. Please try again later.";
        } else {
            this.errorMessage = error.error;
        }
        this.closeNotificationAfterTimeout(); 

        return of<AuthenticationResponse | null>(null);
      }),
      tap((response: AuthenticationResponse | null) => {
        if (response) {
          const tokenValidUntil = response.tokenValidUntil instanceof Date ? response.tokenValidUntil : new Date(response.tokenValidUntil);
          const refreshTokenValidUntil = response.refreshTokenValidUntil instanceof Date ? response.refreshTokenValidUntil : new Date(response.refreshTokenValidUntil);
          localStorage.setItem('token', response.token);
          localStorage.setItem('refreshToken', response.refreshToken);
          localStorage.setItem('tokenValidUntil', tokenValidUntil.toISOString());
          localStorage.setItem('refreshTokenValidUntil', refreshTokenValidUntil.toISOString());
          console.log(response);
        }
      })
    ).subscribe((response: AuthenticationResponse | null) => {
      if (!response) {
        console.log('Error occurred during login');
        return;
      }
    
      this.router.navigate(['/logout']);
    });

    console.log(jsonData);
  }

  loadInstitutions(): void {
    this.dataService.getInstitutions().subscribe(
      (data: Institution[]) => {
        this.institutions = data;
        console.log(this.institutions);
      },
      (error: any) => {
        console.error('Hiba történt az intézmények betöltésekor.', error);
      }
    );
  }

  getErrorMessage() {
    if (this.loginForm.controls['neptun'].errors?.['required']) {
      return 'Neptun code is required.';
    }else if (this.loginForm.controls['password'].errors?.['required']) {
      return 'Password is required.';
    }else if (this.loginForm.controls['institutionSelect'].errors?.['required']) {
      return 'University selection is required.';
    }else if (this.loginForm.controls['role'].errors?.['required']) {
      return 'Role selection is required.';
    }else
    return 'Please fill in all fields.';
  }


}