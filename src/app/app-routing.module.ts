import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { MyDataComponent } from './my-data/my-data.component';
import { TeamManagementComponent } from './team-management/team-management.component';
import { WorkoutStudioComponent } from './workout-studio/workout-studio.component';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { NeptunLoginComponent } from './neptun-login/neptun-login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'neptun-login', component: NeptunLoginComponent, pathMatch: 'full'},
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]  },
  { path: 'myData', component: MyDataComponent, canActivate: [AuthGuard]  },
  { path: 'teamManagement', component: TeamManagementComponent, canActivate: [AuthGuard, RoleGuard] },
  { path: 'workoutStudio', component: WorkoutStudioComponent, canActivate: [AuthGuard, RoleGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
