export interface RefreshTokenResponse {
    token: string;
    tokenValidUntil: string;
  }