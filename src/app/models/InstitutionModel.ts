export interface Institution {
    Languages: string;
    Name: string;
    NeptunMobileServiceVersion: Int16Array;
    OMCode: string;
    Url: string;
}
