export interface AuthenticationResponse {
    token: string;
    refreshToken: string;
    tokenValidUntil: Date;
    refreshTokenValidUntil: Date;
}