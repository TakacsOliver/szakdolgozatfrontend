import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service'; 
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  menuItems = [
    {menuUrl:'/logout',menuName:'Home',menuIcon:'bi bi-house-door-fill'},
    {menuUrl:'/profile',menuName:'Profile',menuIcon:'bi bi-person-fill'},
    {menuUrl:'/myData',menuName:'MyData',menuIcon:'bi bi-clipboard-data-fill'},
    {menuUrl:'/teamManagement',menuName:'Class Management',menuIcon:'bi bi-people-fill'},
    {menuUrl:'/workoutStudio',menuName:'Assessment',menuIcon:'bi bi-pencil-fill'}
  ]

  constructor(
    private router: Router,
    private authService: AuthService,
    private roleService: RoleService,
    private _ngZone: NgZone,
    private http: HttpClient
  ) { }

  public logout(): void {
    this.authService.signOutExternal();
    this.router.navigate(['/']).then(() => window.location.reload());
  }

  ngOnInit(): void {
    this.checkTeacherRole(); // A komponens inicializálása során ellenőrizzük a tanár szerepkört
  }
  private checkTeacherRole(): void {
    this.roleService.isTeacher().subscribe(isTeacher => {
      if (!isTeacher) {
        // Ha a felhasználó nem tanár, akkor eltávolítjuk a menüből a "Class Management" gombot
        this.menuItems = this.menuItems.filter(item => item.menuUrl !== '/teamManagement');
      }
    });
  }

  navigateTo(url: string) {
    if (url === '/logout') {
      this.authService.logout();
      return;
    }
  
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['/login'], { queryParams: { tokenExpired: true } });
      return;
    }
  
    this.authService.validateToken().pipe(
      tap(isValid => {
        if (isValid) {
          console.log("navigate");
          this.router.navigate([url]);
        } else {
          // A token érvénytelen, frissítjük a tokent
          this.authService.refreshToken().pipe(
            catchError(error => {
              console.error('Hiba történt az új tokent frissítése során:', error);
              return throwError(error);
            })
          ).subscribe(
            refreshedToken => {
              // Az új token sikeresen frissítve lett, folytathatjuk a navigációt
              this.router.navigate([url]);
            },
            error => {
              // Ha nem sikerült az új tokent frissíteni, kijelentkeztetjük a felhasználót
              this.authService.logout();
            }
          );
        }
      }),
      catchError(error => {
        console.error('Hiba történt a token ellenőrzése során:', error);
        return throwError(error);
      })
    ).subscribe(() => {
      console.log("menuloaded");
    });
  }
}
