import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { catchError, of, tap, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.authService.validateToken().pipe(
      tap(isValid => {
        if (isValid) {
          console.log("true");
          return true;
        } else {
          this.authService.refreshToken().pipe(
            catchError(error => {
              console.error('Hiba történt az új tokent frissítése során:', error);
              return throwError(error);
            })
          ).subscribe(
            refreshedToken => {
              // Az új token sikeresen frissítve lett, folytathatjuk a navigációt
              this.router.navigate([state.url]);
            },
            error => {
              // Ha nem sikerült az új tokent frissíteni, kijelentkeztetjük a felhasználót
              this.authService.signOutExternal();
              this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
            }
          );
          console.log("false");
          return false;
        }
      }),
      catchError(error => {
        console.error('Hiba történt a token ellenőrzése során:', error);
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        console.log("false");
        return of(false);
      })
    );
    }
}