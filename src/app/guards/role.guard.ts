import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { RoleService } from '../services/role.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private roleService: RoleService,
    private router: Router
  ) {}

  canActivate(): Observable<boolean> {
    return this.roleService.isTeacher().pipe(
      map(isTeacher => {
        if (!isTeacher) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      })
    );
  }
}

