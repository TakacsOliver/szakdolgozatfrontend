import { Component, OnInit } from '@angular/core';
import { WorkoutService } from '../services/workout.service';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-my-data',
  templateUrl: './my-data.component.html',
  styleUrls: ['./my-data.component.css']
})
export class MyDataComponent implements OnInit {
  currentPage = 1;
  itemsPerPage = 5;
  dataRows: any[] = [];
  exercises: any[] = [];
  showDetailsTable = false;
  selectedWorkoutName: string = '';
  selectedRowIndex = -1;


  constructor(
    private workoutService: WorkoutService,
    private authService: AuthService,
    private router: Router,
    ) {}

  ngOnInit() {
    this.fetchData();
  }
  

  fetchData(): void {
    const sortBy = 'startDate';
    const sortDirection = 'asc';
    const filterBy = '';
    const filterValue = '';


  this.authService.validateToken().pipe(
        tap(isValid => {
          if (isValid) {
            this.workoutService
            .getPaginatedWorkouts(
              this.currentPage,
              this.itemsPerPage,
              sortBy,
              sortDirection,
              filterBy,
              filterValue
            )
            .subscribe(data => {
              this.dataRows = data;
              console.log('Data received:', this.dataRows);
            }, error => {
              console.error('Hiba történt az adatok lekérése során:', error);
            });
        } else {
          this.authService.refreshToken().pipe(
            catchError(error => {
              console.error('Hiba történt az új tokent frissítése során:', error);
              return throwError(error);
            })
          ).subscribe(
            refreshedToken => {
              this.fetchData();
            },
            error => {
              this.authService.logout();
            }
          );
        }
      }),
      catchError(error => {
        console.error('Hiba történt a token ellenőrzése során:', error);
        return throwError(error);
      })
    ).subscribe(() => {
      console.log("Successfully");
    });
  }

  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
      this.fetchData();
    }
  }

  nextPage(): void {
    if (this.dataRows.length >= this.itemsPerPage) {
      this.currentPage++;
      this.fetchData();
    }
  }

  showDetails(workoutId: string, workoutName: string): void {
    console.log('ID:', workoutId);
    this.selectedWorkoutName = workoutName;
  
    this.authService.validateToken().pipe(
      tap(isValid => {
        if (isValid) {
          this.workoutService
            .getWorkoutDetails(workoutId)
            .subscribe(data => {
              this.exercises = data;
              this.showDetailsTable = true;
              console.log('Data received:', this.exercises);
            }, error => {
              console.error('Hiba történt az adatok lekérése során:', error);
            });
        } else {
          this.authService.refreshToken().pipe(
            catchError(error => {
              console.error('Hiba történt az új tokent frissítése során:', error);
              return throwError(error);
            })
          ).subscribe(
            refreshedToken => {
              this.workoutService
                .getWorkoutDetails(workoutId)
                .subscribe(data => {
                  this.exercises = data;
                  this.showDetailsTable = true;
                  console.log('Data received:', this.exercises);
                }, error => {
                  console.error('Hiba történt az adatok lekérése során:', error);
                });
            },
            error => {
              this.authService.logout();
            }
          );
        }
      }),
      catchError(error => {
        console.error('Hiba történt a token ellenőrzése során:', error);
        return throwError(error);
      })
    ).subscribe(() => {
      console.log("Successfully");
    });
  }

  goBack(): void {
    this.showDetailsTable = false; 
  }

}