import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { CredentialResponse, PromptMomentNotification } from 'google-one-tap';
import { AuthService } from '../services/auth.service';
import { EMPTY, catchError, first, of, firstValueFrom, tap, finalize, from, map } from 'rxjs';
import { AuthenticationResponse } from '../models/AuthenticationResponseModel';
import { DataService } from '../services/data.service';
import { Institution } from '../models/InstitutionModel';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{
  institutions: Institution[] = [];
  selectedInstitution: string = '';
  tokenExpired: boolean = false;

  constructor(
    private router: Router,
    private service: AuthService,
    private _ngZone: NgZone,
    private route: ActivatedRoute
  ) { }


  redirectToPage() {
    this.router.navigate(['/neptun-login']);
  }

  private isUserLoggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params['tokenExpired']) {
        this.tokenExpired = true;
      }
    });
    if (this.isUserLoggedIn()) {
      this.router.navigate(['/logout']); 
    }
    // @ts-ignore
    window.onGoogleLibraryLoad = () => {
      // @ts-ignore
      google.accounts.id.initialize({
       client_id: '694033662565-kdiqe2dnsbelttu50d6u0kk5esdkhbgn.apps.googleusercontent.com',
        callback: this.handleCredentialResponse.bind(this),
        auto_select: false,
        cancel_on_tap_outside: true
      });
      // @ts-ignore
      google.accounts.id.renderButton(
      // @ts-ignore
      document.getElementById("buttonDiv"),
        { theme: "outline", size: "large", width: "100%" } 
      );
      // @ts-ignore
      google.accounts.id.prompt((notification: PromptMomentNotification) => {});
    };
    
  }


  handleCredentialResponse(response: CredentialResponse) {
    from(this.service.LoginWithGoogle(response.credential)).pipe(
      first(),
      catchError((error) => {
        console.log(error);
        return of(null);
      }),
      map((x: AuthenticationResponse) => {
        const authResponse: AuthenticationResponse = {
          token: x.token,
          refreshToken: x.refreshToken,
          tokenValidUntil: new Date(x.tokenValidUntil),
          refreshTokenValidUntil: new Date(x.refreshTokenValidUntil)
        };
        return authResponse;
      }),
      tap((authResponse: AuthenticationResponse) => {
        localStorage.setItem('token', authResponse.token);
        localStorage.setItem('refreshToken', authResponse.refreshToken);
        localStorage.setItem('tokenValidUntil', authResponse.tokenValidUntil.toISOString());
        localStorage.setItem('refreshTokenValidUntil', authResponse.refreshTokenValidUntil.toISOString());
      })
    ).subscribe((x: AuthenticationResponse) => {
      if (!x) {
        console.log('Error occurred during login');
        return;
      }
    
      this._ngZone.run(() => {
        this.router.navigate(['/logout']);
      });
    });
  }

}