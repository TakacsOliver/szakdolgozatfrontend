import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-workout-studio',
  templateUrl: './workout-studio.component.html',
  styleUrls: ['./workout-studio.component.css']
})
export class WorkoutStudioComponent {
  dataRows: any[] = [];
  workouts: any[] = [];
  details: any[] = [];
  lessons: any[] = [];
  students: any[] = [];
  studentsForSelectedWorkout: any[] = []; // Diákok az aktuális órához
  selectedStudentDetails: any[] = []; // Kiválasztott diák részletei
  selectedWorkoutDetails: any[] = []; // Kiválasztott óra részletei
  selectedLessons: any[] = []; // Kiválasztott óra részletei
  currentPage = 1;
  itemsPerPage = 10;
  showDetailsTable = false;
  selectedRowIndex = -1;
  selectedLesson: any = null; // Kiválasztott óra
  selectedLessonId: any = null;
  selectedWorkout: any = null; 
  selectedStudent: any = null; // Kiválasztott diák
  grades: number[] = [1, 2, 3, 4, 5];
  currentLevel: 'home' | 'lessons' | 'students' | 'workouts' | 'studentDetails' |'workoutDetails'  = 'home';

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getLessons().subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );

  }
  
  goBacktoCourses(): void {
    this.dataService.getLessons().subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );
    this.currentLevel = 'home';
  }

  showWorkoutStudents(lesson: any): void {
    this.currentLevel = 'students';
    console.log(lesson);
    console.log(lesson.id);
    this.selectedLesson = lesson;
    this.dataService.getStudents(lesson.id).subscribe(
      (data) => {
      this.students = data;
      console.log(this.students);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );

  }

  showLessons(student: any): void {
    this.currentLevel = 'lessons';
    this.selectedStudent = student;
    console.log(this.selectedStudent);
    console.log(this.selectedStudent.userId);
    this.dataService.getWorkouts(student.userId, this.selectedLesson.id).subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );
  }

  saveGrade(student: any, lesson: any) {
    const grade = student.grade; 
    console.log('Saving grade for student:', student);

    this.dataService.saveUserGrade(student.userId, lesson.id, grade).subscribe(
      (response) => {
        console.log();
        console.log('Az adatok sikeresen elmentve.');
      },
      (error) => {
        console.error('Hiba történt a jegy mentésekor:', error);
      }
    );

  } 

  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage(): void {
    if (this.dataRows.length > this.itemsPerPage) {
      this.currentPage++;
    }
  }
}