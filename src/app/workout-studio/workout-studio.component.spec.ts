import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutStudioComponent } from './workout-studio.component';

describe('WorkoutStudioComponent', () => {
  let component: WorkoutStudioComponent;
  let fixture: ComponentFixture<WorkoutStudioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutStudioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkoutStudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
