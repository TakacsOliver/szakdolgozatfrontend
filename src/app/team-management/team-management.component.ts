import { Component } from '@angular/core';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-team-management',
  templateUrl: './team-management.component.html',
  styleUrls: ['./team-management.component.css']
})
export class TeamManagementComponent {
  dataRows: any[] = [];
  workouts: any[] = [];
  details: any[] = [];
  lessons: any[] = [];
  students: any[] = [];
  studentsForSelectedWorkout: any[] = []; // Diákok az aktuális órához
  selectedStudentDetails: any[] = []; // Kiválasztott diák részletei
  selectedWorkoutDetails: any[] = []; // Kiválasztott óra részletei
  selectedLessons: any[] = []; // Kiválasztott óra részletei
  currentPage = 1;
  itemsPerPage = 10;
  showDetailsTable = false;
  selectedRowIndex = -1;
  selectedLesson: any = null; // Kiválasztott óra
  selectedLessonId: any = null;
  selectedWorkout: any = null; 
  selectedStudent: any = null; // Kiválasztott diák
  grades = [1, 2, 3, 4, 5];
  currentLevel: 'home' | 'lessons' | 'students' | 'workouts' | 'studentDetails' |'workoutDetails'  = 'home';

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.dataService.getLessons().subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );

  }
  
  goBacktoCourses(): void {
    this.dataService.getLessons().subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );
    this.currentLevel = 'home';
  }

  showWorkoutStudents(lesson: any): void {
    this.currentLevel = 'students';
    console.log(lesson);
    console.log(lesson.id);
    this.selectedLesson = lesson;
    this.dataService.getStudents(lesson.id).subscribe(
      (data) => {
      this.students = data;
      console.log(this.students);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );

  }

  goBackToStudents(): void {
    this.currentLevel = 'students';
    this.showWorkoutStudents(this.selectedLesson);
  }


  showLessons(student: any): void {
    this.currentLevel = 'lessons';
    this.selectedStudent = student;
    console.log(this.selectedStudent);
    console.log(this.selectedStudent.userId);
    this.dataService.getWorkouts(student.userId, this.selectedLesson.id).subscribe(
      (data) => {
      this.lessons = data;
      console.log(this.lessons);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );
  }

  goBackToWorkouts(): void {
    this.currentLevel = 'lessons';
    this.showLessons(this.selectedStudent);
  }

  showStudentDetails(workout: any): void {
    this.currentLevel = 'studentDetails';
    this.selectedWorkout = workout;
    console.log(this.selectedWorkout);
    console.log(this.selectedWorkout.workoutId);
    console.log(this.selectedStudent);
    console.log(this.selectedStudent.userId);

    this.dataService.getDetails(this.selectedStudent.userId, workout.workoutId).subscribe(
      (data) => {
      this.selectedStudentDetails = data;
      console.log(this.selectedStudentDetails);
      },
      (error) => {
        console.error('Hiba történt a Kurzusok lekérésekor:', error);
      }
    );

  }

  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage(): void {
    if (this.dataRows.length > this.itemsPerPage) {
      this.currentPage++;
    }
  }
}
