import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../services/data.service';
import { DatePipe } from '@angular/common';
import { AuthService } from '../services/auth.service';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [DatePipe]
})
export class ProfileComponent implements OnInit {
  profileForm!: FormGroup;
  isLoading = false;
  profileData: any;
  isProfileUpdateSuccessful = false;
  errorMessages: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private authService: AuthService,
    private datePipe: DatePipe
    ) {}

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      neptun: [{ value: '', disabled: true }],
      birthday: [''],
      firstName: [''],
      lastName: [''],
      phoneNumber: [''],
      location: [''],
      email: ['']
    });

    this.loadProfile();
  }

  loadProfile(): void {
    this.isLoading = true;
    this.dataService.getProfile().subscribe(
      (profile) => {
        this.isLoading = false;
        console.log('Profiladatok:', profile);

        const formattedBirthday = this.datePipe.transform(profile.birthday, 'yyyy-MM-dd');

        this.profileForm.patchValue({
          neptun: profile.neptun,
          birthday: formattedBirthday,
          firstName: profile.firstName,
          lastName: profile.lastName,
          phoneNumber: profile.phoneNumber,
          location: profile.location,
          email: profile.email
        });
      },
      (error) => {
        this.isLoading = false;
        console.error('Hiba történt a profiladatok lekérésekor:', error);
      }
    );
  }

  updateProfile(): void {
    this.isLoading = true;

    const token = localStorage.getItem('token');
    const profileData: any = {
      token: token
    };

    this.errorMessages = [];

    if (this.profileForm.get('email')?.invalid) {
      this.errorMessages.push('Invalid email. Format: xxx@xxx.xx');
    } else {
      profileData.email = this.profileForm.get('email')?.value;
    }

    for (const field in this.profileForm.controls) {
      const value = this.profileForm.get(field)?.value;

      if (value !== null && value !== '') {
        profileData[field] = value;
      } else {
        profileData[field] = null;
      }
    }

    if (this.errorMessages.length > 0) {
      this.isLoading = false;
      return;
    }

    console.log('Küldött adatok:', profileData);
    this.authService.validateToken().pipe(
      tap(isValid => {
        if (isValid) {
          this.dataService.updateProfile(profileData).subscribe(
            (response) => {
              this.isLoading = false;
              console.log('A profil sikeresen frissítve.');
              this.isProfileUpdateSuccessful = true;
            },
            (error) => {
              this.isLoading = false;
              console.error('Hiba történt a profil frissítésekor:', error);
            }
          );
        } else {
          this.authService.refreshToken().pipe(
            catchError(error => {
              console.error('Hiba történt az új tokent frissítése során:', error);
              return throwError(error);
            })
          ).subscribe(
            refreshedToken => {
              this.dataService.updateProfile(profileData).subscribe(
                (response) => {
                  this.isLoading = false;
                  console.log('A profil sikeresen frissítve.');
                  this.isProfileUpdateSuccessful = true;
                },
                (error) => {
                  this.isLoading = false;
                  console.error('Hiba történt a profil frissítésekor:', error);
                }
              );
            },
            error => {
              this.authService.logout();
            }
          );
        }
      }),
      catchError(error => {
        console.error('Hiba történt a token ellenőrzése során:', error);
        return throwError(error);
      })
    ).subscribe(() => {
      console.log("Successfully");
    });
  }
}
