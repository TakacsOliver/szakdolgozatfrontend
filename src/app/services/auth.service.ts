import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Observable, throwError, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { RefreshTokenResponse } from '../models/RefreshTokenResponseModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loginSuccess: boolean = false;

  isTeacher() {
    throw new Error('Method not implemented.');
  }

  private apiUrl = environment.apiUrl;

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    withCredentials: false
  };

  constructor(private httpClient: HttpClient, private router: Router) {}

  public signOutExternal(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('tokenValidUntil');
    localStorage.removeItem('refreshTokenValidUntil');
    console.log('Token deleted');

  }
  
  public logout(): void {
    this.signOutExternal();
    this.router.navigate(['/'], { queryParams: { tokenExpired: true } }).then(() => window.location.reload());
  }

  public isLoggedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  validateToken(): Observable<boolean> {
    const token = localStorage.getItem('token');
    if (token) {
      const decodedToken = this.decodeToken(token); // Token dekódolása
      const tokenExpirationDate = new Date(decodedToken.exp * 1000); // Token expirálási időpontja (másodpercben)
      const currentDateTime = new Date(); // Jelenlegi időpont

      console.log('Token valid until:', tokenExpirationDate);
      console.log('Current date:', currentDateTime);
      // Az érvényesség ellenőrzése
      const isValid = tokenExpirationDate > currentDateTime;

      return of(isValid);
    } else {
      // Hiba dobása, ha a token null
      return throwError('A token értéke null.');
    }
  }

  private decodeToken(token: string): any {
    const tokenParts = token.split('.');
    if (tokenParts.length === 3) {
      const payload = tokenParts[1];
      const decodedPayload = atob(payload);
      return JSON.parse(decodedPayload);
    }
    return null;
  }

  refreshToken(): Observable<RefreshTokenResponse> {
    const refreshToken = localStorage.getItem('refreshToken');
    if (refreshToken) {
      const url = this.apiUrl + 'api/token/refreshToken';
      const payload = '"' + refreshToken + '"';
      console.log('Kérés küldése:', url);
      console.log('Kérés törzse:', payload );
      return this.httpClient.post<RefreshTokenResponse>(this.apiUrl + 'api/token/refreshToken', payload, this.httpOptions).pipe(
        tap((response: RefreshTokenResponse) => {
          // Az új token sikeresen frissítve lett, beállítjuk a localStorage-ba
          localStorage.setItem('token', response.token);
          localStorage.setItem('tokenValidUntil', response.tokenValidUntil);
        }),
        catchError((error) => {
          console.error('Hiba történt az új tokent frissítése során:', error);
          return throwError(error);
        })
      );
    } else {
      // Hiba dobása, ha a refresh token null
      return throwError('A refresh token értéke null.');
    }
  }

  LoginWithGoogle(credentials: string): Observable<any> {
    return this.httpClient.post(this.apiUrl + 'LoginWithGoogle', JSON.stringify(credentials), this.httpOptions);
  }

  clearLoginSuccess(): void {
    this.loginSuccess = false;
  }

}