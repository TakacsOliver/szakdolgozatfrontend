import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkoutService {

  public dataUrl = environment.apiUrl + "api/MyData/my-data";
  public detailsUrl = environment.apiUrl + "api/Mydata/Exercise";
  public RefreshUrl = environment.apiUrl + "api/token/refreshToken";

  constructor(private http: HttpClient) { }

  getPaginatedWorkouts(pageNumber: number, itemsPerPage: number, sortBy: string, sortDirection: string, filterBy: string, filterValue: string): Observable<any> {
    const url = this.dataUrl;
    const token = localStorage.getItem('token');
    const body = {
      token: token,
      pageNumber: pageNumber.toString(),
      itemsPerPage: itemsPerPage.toString(),
      sortBy: sortBy,
      sortDirection: sortDirection,
      filterBy: filterBy,
      filterValue: filterValue
    };

    return this.http.post<any>(url, body);
  }

  getWorkoutDetails(workoutId: string): Observable<any> {
    const url = this.detailsUrl;
    const token = localStorage.getItem('token');

    const body = {
      token: token,
      workoutId: workoutId
    };


    return this.http.post<any>(url, body);
  }

  refreshToken(refreshToken: string): Observable<any> {
    const url = this.RefreshUrl;

    const body = {
      refreshToken
    };

    return this.http.post<any>(url, body);
  }

  
}
