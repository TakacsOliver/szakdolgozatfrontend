import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {


  private readonly apiUrl = environment.apiUrl;
  private readonly roleEndpoint = 'api/Role/user';
  private readonly teacherRoleName = 'teacher';
  
  constructor(  private http: HttpClient) {}

  getUserRoles(): Observable<any> {
    const token = localStorage.getItem('token');
    const body = { token };
    const url = `${this.apiUrl}${this.roleEndpoint}`;
    
    console.log(token);
    console.log(body);
    console.log(url);


    return this.http.post<any>(url, body);
  }

  isTeacher(): Observable<boolean> {
    return this.getUserRoles().pipe(
      map(userRoles => userRoles.includes(this.teacherRoleName))
    );
  }

}
