import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Institution } from '../models/InstitutionModel';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private institutionsUrl = 'assets/Institutes.json';
  public profileUrl = environment.apiUrl + "api/Profile";
  public coursesUrl = environment.apiUrl + "api/Class";
  public studentsUrl = environment.apiUrl + "api/Class";
  public lessonsUrl = environment.apiUrl + "api/Class";
  public detailsUrl = environment.apiUrl + "api/Class";

  constructor(private http: HttpClient) { }

  getInstitutions(): Observable<Institution[]> {
    return this.http.get<Institution[]>(this.institutionsUrl);
  }

  getProfile(): Observable<any> {
    const token = localStorage.getItem('token');
    const body = { token };
    const url = `${this.profileUrl}/get-profile`; 

    console.log('Token:', token);
    console.log('URL:', url);
    console.log('Body:', body);
    return this.http.post<any>(url, body);
  }

  updateProfile(profileData: any) {
    const url = `${this.profileUrl}/update-profile`; // URL létrehozása

    console.log('URL:', url);
    console.log('Profile Data:', profileData);
    return this.http.put<any>(url, profileData, { responseType: 'json' });
  }

  getLessons(): Observable<any[]> {
    const token = localStorage.getItem('token');
    const body = { token };
    const url = `${this.lessonsUrl}/get-lessons`; 

    return this.http.post<any>(url, body);
  }

  getStudents(lessonId: string): Observable<any[]> {
    const body = { lessonId };
    const url = `${this.studentsUrl}/get-students`; 
  
    return this.http.post<any>(url, body);
  }

  getWorkouts(studentId: string, lessonId: string): Observable<any[]> {
    const token = localStorage.getItem('token');
    const body = { studentId,lessonId };
    const url = `${this.lessonsUrl}/get-workouts`; 

    return this.http.post<any>(url, body);
  }

  getDetails(userId: string, workoutId: string): Observable<any[]> {
    const body = { userId,workoutId };
    const url = `${this.detailsUrl}/get-details`; 

    return this.http.post<any>(url, body);
  }

  saveUserGrade(userId: string, lessonId: string, grade: number): Observable<any[]> {
    const body = { userId,lessonId,grade};
    const url = `${this.detailsUrl}/save-grade`; 
    console.log(body);

    return this.http.put<any>(url, body);
  }
}