import { Component, NgZone, OnInit } from '@angular/core';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  userRoles: string[] = [];

  constructor(private roleService: RoleService) { }

  ngOnInit(): void {
    this.roleService.getUserRoles().subscribe(
      (roles: string[]) => {
        this.userRoles = roles;
      },
      (error) => {
        console.error('Error fetching user roles:', error);
      }
    );
  }
}